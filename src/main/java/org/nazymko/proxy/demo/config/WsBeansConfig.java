package org.nazymko.proxy.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.nazymko.proxy.demo.ws.ProxyRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
public class WsBeansConfig {
    @Value("${PROP_NAME}")
    public String propertyname;

    @PostConstruct
    public void postConstruct() {
        log.info("postConstruct: " + propertyname);
    }

    @Bean
    ProxyRegistry proxyRegistry() {
        return new ProxyRegistry();
    }


}
