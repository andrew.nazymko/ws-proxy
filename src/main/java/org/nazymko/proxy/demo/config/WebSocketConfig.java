package org.nazymko.proxy.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Bean
    public WebSoketProxyHandler websocketMessageHandler() {
        return new WebSoketProxyHandler();
    }

    @Autowired
    WebSoketProxyHandler handler;

    /**
     * Register {@link WebSoketProxyHandler}s including SockJS fallback options if desired.
     *
     * @param registry
     */

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(handler, "/proxy")
                .setAllowedOrigins("*");
    }
}
