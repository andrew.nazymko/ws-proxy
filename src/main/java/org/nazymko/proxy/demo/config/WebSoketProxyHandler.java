package org.nazymko.proxy.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.nazymko.proxy.demo.utils.QueryUtils;
import org.nazymko.proxy.demo.ws.Connector;
import org.nazymko.proxy.demo.ws.ProxyRegistry;
import org.nazymko.proxy.demo.ws.WebSocketClientHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.socket.*;

import java.util.List;
import java.util.Map;

@Slf4j
public class WebSoketProxyHandler implements WebSocketHandler {

    @Value("${PROP_NAME}")
    public String propertyname;
    @Autowired
    ProxyRegistry proxyRegistry;
    @Autowired
    Connector connector;
    @Autowired
    QueryUtils queryUtils;

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        String query = webSocketSession.getUri().getQuery();
        Map<String, List<String>> params = queryUtils.splitQuery(query);
        log.info(params.toString());

        List<String> properties = params.get(propertyname);
        if (properties == null || properties.isEmpty()) {
            String formattedMessage = String.format("Property %s not found in query parameters %s", propertyname, query);
            log.error(formattedMessage);
            webSocketSession.sendMessage(new TextMessage(formattedMessage));
            throw new IllegalArgumentException(formattedMessage);
        }
        String value = properties.get(0);

        WebSocketClientHandler handler = connector.connect(value, webSocketSession.getId());

        proxyRegistry.register(handler, webSocketSession);

    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        log.info("handleMessage : " + String.valueOf(webSocketMessage.getPayload()));

        proxyRegistry.onMessageFromClient(webSocketSession.getId(), String.valueOf(webSocketMessage.getPayload()));
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        proxyRegistry.remove(webSocketSession.getId());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        proxyRegistry.remove(webSocketSession.getId());

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
