package org.nazymko.proxy.demo.ws;

public interface MessageConsumer {
    void consume(String sessionId,String payload);
}
