package org.nazymko.proxy.demo.ws;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.HashMap;

@Slf4j
@Component
public class ProxyRegistry {

    private final HashMap<String, WebSocketClientHandler> register = new HashMap<>();

    public void register(WebSocketClientHandler handler, WebSocketSession webSocketSession) {
        handler.setClientSession(webSocketSession);
        handler.setConsumer(new MessageConsumer() {
            @Override
            public void consume(String sessionId, String payload) {
                try {
                    webSocketSession.sendMessage(new TextMessage(payload));
                } catch (IOException e) {
                    remove(sessionId);
                    e.printStackTrace();
                }
            }
        });

        register.put(webSocketSession.getId(), handler);

    }

    public void onMessageFromClient(String sessionId, String payload) {
        register.get(sessionId).sendMessage(payload);
    }

    public void remove(String sessionId) {
        WebSocketClientHandler removed = register.remove(sessionId);
        log.info("Session removed:" + removed);
    }


}
